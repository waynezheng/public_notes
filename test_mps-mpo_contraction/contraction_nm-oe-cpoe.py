import numpy as np
import cupy as cp
import opt_einsum as oe
from scipy.stats import unitary_group as ug

import timeit
import cpuinfo

DIM_PHYS = 2

class FiniteOpenMPS:
    # class contructor
    # class members: numsite, dim_bond, dims, tensors
    def __init__ (self, num_site, chi):
        self.num_site = num_site
        self.dim_bond = chi
        # determine the bond dimension of the (num_site+1) bonds
        dims = []
        # from left to center
        d = 1
        dims.append (d)
        d = d*DIM_PHYS
        for l in range (num_site // 2):
            if d < chi:
                dims.append (d)
                d = d*DIM_PHYS
            else:
                dims.append (chi)
        # from right to center
        d = 1
        dims.append (d)
        d = d*DIM_PHYS
        for l in range (num_site-2, (num_site // 2)-1, -1):
            if d < chi:
                dims.insert ((num_site // 2)+1, d)
                d = d*DIM_PHYS
            else:
                dims.insert ((num_site // 2)+1, chi)
        self.dims = dims
        self.tensors = []
        self.cp_tensors = []
    # generate a random right-canonical MPS
    def RandomRightCanonical (self):
        for site in range (self.num_site):
            dim_alpha, dim_beta = self.dims[site], self.dims[site+1]
            u_mat = ug.rvs (DIM_PHYS*dim_beta)[:dim_alpha]
            right_tensor = u_mat.reshape (dim_alpha, DIM_PHYS, dim_beta)
            # permute the physical bond to the front
            self.tensors.append (np.transpose (right_tensor, (1, 0, 2)))
        for site in range (self.num_site):
            self.cp_tensors.append (cp.asarray (self.tensors[site]))
    # write the MPS to the file
    def WriteToFile (self, file_name):
        fr = open (file_name, 'wb')
        pickle.dump (self.tensors, fr)
        fr.close ()
    # read from the file
    def ReadFromFile (self, file_name):
        tensor_list = pickle.load (open (file_name, 'rb'))
        if self.num_site != len (tensor_list):
            print ('error: mismatched size of the loaded MPS!')
        else:
            self.tensors = tensor_list

class XYZHamiltonianMPO:
    def __init__ (self, num_site, Jx, Jy, Jz, h):
        self.num_site = num_site
        self.Jx = Jx
        self.Jy = Jy
        self.Jz = Jz
        self.h = h
        self.tensors = []
        self.cp_tensors = []
        # Pauli matrices
        Id = np.matrix ([[1.0, 0.0], [0.0, 1.0]])
        Sx = np.matrix ([[0.0, 1.0], [1.0, 0.0]])
        Sy = np.matrix ([[0.0, -1.j], [1.j, 0.0]])
        Sz = np.matrix ([[1.0, 0.0], [0.0, -1.0]])
        # mpo frame
        dim_mpo = 5
        mpo_frame = np.matrix (np.zeros ((dim_mpo, dim_mpo), dtype=float))
        w_bulk = np.matrix (np.zeros ((dim_mpo*DIM_PHYS, dim_mpo*DIM_PHYS), dtype=complex))
        ver_list = [Id, self.Jx*Sx, self.Jy*Sy, self.Jz*Sz]
        hor_list = [Sx, Sy, Sz, Id]
        # set the onsite term
        mpo_frame[4, 0] = 1.0
        w_bulk += np.kron (mpo_frame, self.h*Sz)
        # set the nearest interaction terms
        for i in range (dim_mpo-1):
            mpo_frame.fill (0.0)
            mpo_frame[i, 0] = 1.0
            w_bulk += np.kron (mpo_frame, ver_list[i])
        for i in range (dim_mpo-1):
            mpo_frame.fill (0.0)
            mpo_frame[dim_mpo-1, i+1] = 1.0
            w_bulk += np.kron (mpo_frame, hor_list[i])
        # boundary vectors
        bdv_left = np.array ([[0.0, 0.0, 0.0, 0.0, 1.0]])
        bdv_right = np.transpose (np.array ([[1.0, 0.0, 0.0, 0.0, 0.0]]))
        w_head = np.kron (bdv_left, Id) @ w_bulk
        w_tail = w_bulk @ np.kron (bdv_right, Id)
        # reshape and permute to mpo tensors
        mpo_bulk = np.transpose (np.array (w_bulk).reshape (dim_mpo, DIM_PHYS, dim_mpo, DIM_PHYS),
                (1, 3, 0, 2)).copy ()
        mpo_head = np.transpose (np.array (w_head).reshape (1, DIM_PHYS, dim_mpo, DIM_PHYS),
                (1, 3, 0, 2)).copy ()
        mpo_tail = np.transpose (np.array (w_tail).reshape (dim_mpo, DIM_PHYS, 1, DIM_PHYS),
                (1, 3, 0, 2)).copy ()
        self.tensors.append (mpo_head)
        for site in range (self.num_site-2):
            self.tensors.append (mpo_bulk)
        self.tensors.append (mpo_tail)
        for site in range (self.num_site):
            self.cp_tensors.append (cp.asarray (self.tensors[site]))
    # simple method
    def ExpectionValueNM (self, mps):
        tn = np.array([1.0], dtype=np.float64).reshape (1, 1, 1, 1, 1, 1)
        for site in range (self.num_site):
            # site_tensor: * stands for a MPS; O stands for a MPO
            # --*--
            #   |
            # --O--
            #   |
            # --*--
            site_tensor = np.tensordot (np.conjugate (mps.tensors[site]), self.tensors[site],
                    axes=((0), (0)))
            site_tensor = np.tensordot (site_tensor, mps.tensors[site],
                    axes=((2), (0)))
            tn = np.tensordot (tn, site_tensor,
                    axes=((1, 3, 5), (0, 2, 4)))
            tn = np.transpose (tn, (0, 3, 1, 4, 2, 5))
        return tn.reshape (1)[0]
    # opt_einsum
    def ExpectionValueOE (self, mps):
        tn = np.array ([1.0], dtype=np.float).reshape (1, 1, 1, 1, 1, 1)
        for site in range (self.num_site):
            # contract tn, mps and mpo
            # a----d,d--*--h
            #           |g,g
            # b----e,e--*--j
            #           |i,i
            # c----f,f--*--k
            tn = oe.contract ('abcdef,gdh,giej,ifk',
                    tn,
                    np.conjugate (mps.tensors[site]),
                    self.tensors[site],
                    mps.tensors[site],
                    memory_limit=-1)
        return tn.reshape (1)[0]
    # opt_sum+cupy
    def ExpectionValueCupyOE (self, mps):
        #  mpo_tensors = cp.asarray (self.tensors)
        #  mps_tensors = cp.asarray (mps.tensors)
        tn = cp.array ([1.0], dtype=np.float64).reshape (1, 1, 1, 1, 1, 1)
        for site in range (self.num_site):
            # contract tn, mps and mpo
            # a----d,d--*--h
            #           |g,g
            # b----e,e--*--j
            #           |i,i
            # c----f,f--*--k
            tn = oe.contract ('abcdef,gdh,giej,ifk',
                    tn,
                    cp.conj (mps.cp_tensors[site]),
                    self.cp_tensors[site],
                    mps.cp_tensors[site],
                    memory_limit=-1, backend='cupy')
        return tn.reshape (1)[0]
    # opt_sum graph
    def ExpectionValueGraphOE (self, mps):
        einsum_str = ''
        # build the first tensor block, which consists of three tensors
        # --*--
        #   |
        # --*--
        #   |
        # --*--
        site = 0
        # upper MPS tensor
        u0, u1, u2 = (oe.get_symbol (i) for i in (site+0, site+1, site+2))
        # middle MPO tensor
        m0, m1, m2, m3 = (oe.get_symbol (i) for i in (site+0, site+3, site+4, site+5))
        # lower MPS tensor
        l0, l1, l2 = (oe.get_symbol (i) for i in (site+3, site+6, site+7))
        einsum_str += '{}{}{},{}{}{}{},{}{}{},'.format (u0, u1, u2, m0, m1, m2, m3, l0, l1, l2)
        # build the rest tensor blocks, consecutively to be be contracted to the first one
        # --*--
        #   |
        # --*--
        #   |
        # --*--
        for site in range (1, self.num_site):
            bond = site*8
            u0, u1, u2 = (oe.get_symbol (i) for i in (bond+0, (site-1)*8+2, bond+2))
            m0, m1, m2, m3 = (oe.get_symbol (i) for i in (bond+0, bond+3, (site-1)*8+5, bond+5))
            l0, l1, l2 = (oe.get_symbol (i) for i in (bond+3, (site-1)*8+7, bond+7))
            if site != self.num_site-1:
                einsum_str += '{}{}{},{}{}{}{},{}{}{},'.format (u0, u1, u2, m0, m1, m2, m3, l0, l1, l2)
            else:
                einsum_str += '{}{}{},{}{}{}{},{}{}{}'.format (u0, u1, u2, m0, m1, m2, m3, l0, l1, l2)
        tensor_args = []
        for site in range (self.num_site):
            tensor_args.append (np.conjugate (mps.tensors[site]))
            tensor_args.append (self.tensors[site])
            tensor_args.append (mps.tensors[site])
        res = oe.contract (einsum_str, *tensor_args, optimize='greedy', memory_limit=-1)
        return res.reshape (1)[0]
    def ExpectionValueCupyGraphOE (self, mps):
        einsum_str = ''
        # build the first tensor block, which consists of three tensors
        # --*--
        #   |
        # --*--
        #   |
        # --*--
        site = 0
        # upper MPS tensor
        u0, u1, u2 = (oe.get_symbol (i) for i in (site+0, site+1, site+2))
        # middle MPO tensor
        m0, m1, m2, m3 = (oe.get_symbol (i) for i in (site+0, site+3, site+4, site+5))
        # lower MPS tensor
        l0, l1, l2 = (oe.get_symbol (i) for i in (site+3, site+6, site+7))
        einsum_str += '{}{}{},{}{}{}{},{}{}{},'.format (u0, u1, u2, m0, m1, m2, m3, l0, l1, l2)
        # build the rest tensor blocks, consecutively to be be contracted to the first one
        # --*--
        #   |
        # --*--
        #   |
        # --*--
        for site in range (1, self.num_site):
            bond = site*8
            u0, u1, u2 = (oe.get_symbol (i) for i in (bond+0, (site-1)*8+2, bond+2))
            m0, m1, m2, m3 = (oe.get_symbol (i) for i in (bond+0, bond+3, (site-1)*8+5, bond+5))
            l0, l1, l2 = (oe.get_symbol (i) for i in (bond+3, (site-1)*8+7, bond+7))
            if site != self.num_site-1:
                einsum_str += '{}{}{},{}{}{}{},{}{}{},'.format (u0, u1, u2, m0, m1, m2, m3, l0, l1, l2)
            else:
                einsum_str += '{}{}{},{}{}{}{},{}{}{}'.format (u0, u1, u2, m0, m1, m2, m3, l0, l1, l2)
        tensor_args = []
        for site in range (self.num_site):
            tensor_args.append (cp.conj (mps.cp_tensors[site]))
            tensor_args.append (self.cp_tensors[site])
            tensor_args.append (mps.cp_tensors[site])
        res = oe.contract (einsum_str, *tensor_args, optimize='greedy', memory_limit=-1, backend='cupy')
        return res.reshape (1)[0]

repeat = 10
# lattice parameters
Jx = 1.0
Jy = 1.0
Jz = 0.5
h = 0.5

print (cpuinfo.get_cpu_info ().get ('brand'))

def TestNM (num_site, chi):
    ham_mpo = XYZHamiltonianMPO (num_site, Jx, Jy, Jz, h)
    mps = FiniteOpenMPS (num_site, chi)
    mps.RandomRightCanonical ()
    # nm
    time_list = []
    for i in range (repeat):
        start = timeit.default_timer ()
        res_nm = np. real (ham_mpo.ExpectionValueNM (mps))
        stop = timeit.default_timer ()
        time_list.append ((stop - start)*1000)
    time_nm = np.mean (time_list)
    err_nm = np.std (time_list) / np.sqrt (repeat)
    # OE
    time_list = []
    for i in range (repeat):
        start = timeit.default_timer ()
        res_oe = np. real (ham_mpo.ExpectionValueOE (mps))
        stop = timeit.default_timer ()
        time_list.append ((stop - start)*1000)
    time_oe = np.mean (time_list)
    err_oe = np.std (time_list) / np.sqrt (repeat)
    # CUDA-OE
    time_list = []
    for i in range (repeat):
        start = timeit.default_timer ()
        res_cpoe = cp.real (ham_mpo.ExpectionValueCupyOE (mps))
        stop = timeit.default_timer ()
        time_list.append ((stop - start)*1000)
    time_cpoe = np.mean (time_list)
    err_cpoe = np.std (time_list) / np.sqrt (repeat)
    print (chi, res_nm, res_oe, res_cpoe, time_nm, err_nm, time_oe, err_oe, time_cpoe, err_cpoe)
    return (chi, res_nm, res_oe, res_cpoe, time_nm, err_nm, time_oe, err_oe, time_cpoe, err_cpoe)

def TestCuPy (num_site, chi):
    ham_mpo = XYZHamiltonianMPO (num_site, Jx, Jy, Jz, h)
    mps = FiniteOpenMPS (num_site, chi)
    mps.RandomRightCanonical ()

    time_list = []
    for i in range (repeat):
        start = timeit.default_timer ()
        res_oe = np. real (ham_mpo.ExpectionValueOE (mps))
        stop = timeit.default_timer ()
        time_list.append ((stop - start)*1000)
    time_oe = np.mean (time_list)
    err_oe = np.std (time_list) / np.sqrt (repeat)

    time_list = []
    for i in range (repeat):
        start = timeit.default_timer ()
        res_cpoe = cp.real (ham_mpo.ExpectionValueCupyOE (mps))
        stop = timeit.default_timer ()
        time_list.append ((stop - start)*1000)
    time_cpoe = np.mean (time_list)
    err_cpoe = np.std (time_list) / np.sqrt (repeat)
    print (chi, res_oe, res_cpoe, time_oe, err_oe, time_cpoe, err_cpoe)
    return (chi, res_oe, res_cpoe, time_oe, err_oe, time_cpoe, err_cpoe)

import csv

csv_file = open ('lattice32_nm_oe_cpoe.csv', mode='w')
names = ['chi', 'res_nm', 'res_oe', 'res_cpoe', 'time_nm', 'err_nm', 'time_oe', 'err_oe', 'time_cpoe', 'err_cpoe']
writer = csv.DictWriter (csv_file, fieldnames=names)
writer.writeheader ()

chi = 10
for t in range (40):
    test = TestNM (32, chi)
    writer.writerow ({'chi':test[0],
        'res_nm':test[1],
        'res_oe':test[2],
        'res_cpoe':test[3],
        'time_nm':test[4],
        'err_nm':test[5],
        'time_oe':test[6],
        'err_oe':test[7],
        'time_cpoe':test[8],
        'err_cpoe':test[9]
        })
    chi += 1

csv_file.close ()
