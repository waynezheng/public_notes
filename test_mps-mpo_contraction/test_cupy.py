import cupy as cp
import numpy as np
import timeit

repeat = 200

tensor_a = np.random.rand (32, 256, 512)
tensor_b = np.random.rand (64, 512, 256)

start = timeit.default_timer ()
for i in range (repeat):
    tensor_c = np.tensordot (tensor_a, tensor_b, axes=([1, 2], [2, 1]))
end = timeit.default_timer ()
print ('CPU:', (end-start)*1000, 'ms')

tensor_a_gpu = cp.array (tensor_a)
tensor_b_gpu = cp.array (tensor_b)

start = timeit.default_timer ()
for i in range (repeat):
    tensor_c_gpu = cp.tensordot (tensor_a_gpu, tensor_b_gpu, axes=([1, 2], [2, 1]))
end = timeit.default_timer ()
print ('GPU:', (end-start)*1000, 'ms')
